<?php
/*
CABLES-RNT

Copyright SYRTE -- Observatoire de Paris
contributor(s) : 
Frédéric Meynadier (May 2013) - Frederic.Meynadier@obspm.fr

This software is a computer program whose purpose is to make an inventory
of the cable routing in the Salle d'Exploitation.

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

$Id:$
*/

header('Content-Type: text/html; charset=utf-8');
session_start();

include('./config/settings.php');
include('./include/fonctions.php');
include('./include/affichage.php');

# "rw" stocke le mode (lecture-écriture ou lecture seule)
if (isset($_SESSION["rw"])) {
    if ($_SESSION["rw"]) {    
        $checked = True;    
    }
}
else {
    $_SESSION["rw"] = False;
}



$db = new MyDB();

if (!isset($_SESSION["query"])) {
    $_SESSION["query"] = $db->default_query;
}

# Connection à la base de données
$db->connect_to(DBPATH);

# Identification via l'utilisateur apache
if (isset($_SERVER['REMOTE_USER'])) {
    $_SESSION["user_id"] = $_SERVER['REMOTE_USER'];
}
else {
    $_SESSION["user_id"] = "anonymous";
}

if (isset($_POST['display_mode'])) {
    $mode = $_POST['display_mode'];
}
else {
    $mode = "list";
}


# 'command' permet de piloter les opérations à chaque rafraichissement
# de la page.
if (isset($_POST['command'])) {
    $cmd = $_POST['command'];
}
else {
    $cmd = "default";
}

if (isset($_GET['get_csv'])) {
    $cmd = "get_csv"; 
}

$modify_cable_mode = False;
$get_csv = False;
$put_csv_screen = False;
$add_csv_screen = False;

switch ($cmd) {
case "rw" :
    $_SESSION["rw"] = True;
    break;
case "ro" :
    $_SESSION["rw"] = False;
    break;
case "new_cable":   
    $db->new_cable($_POST["new_num"]);
    break;
case "modify" :
    $modify_cable_id = $_POST["id"];
    $modify_cable_mode = True;
    break;
case "delete" :
    $db->delete_cable($_POST["cable_id"]);
    break;
case "filter" :
    $_SESSION["query"] = $db->filter_query($_POST);
    break;
case "maj" :
    $db->maj($_POST, $_FILES);
    break;
case "clone" :
    $db->clone_cable($_POST);
    break;
case "add_csv" :
    $add_csv_screen = True;
    break;
case "put_csv" :
    $put_csv_screen = True;
    break;
case "put_csv_confirmed" :
    $db->build_from_file($_POST, $_FILES, False);
    break;
case "add_csv_confirmed" :
    $db->build_from_file($_POST, $_FILES, True);
    break;
case "get_csv" :
    $db->dump_csv();
    exit;
}

# On récupère les câbles sélectionnés par la requête
$cable_list = $db->get_list($_SESSION["query"]); 

?>
<!DOCTYPE html> 

<html>
    <head>
        <meta charset="utf-8">
        <title>Cahier des câbles RNT</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div id="wrapper">
            <header>
                <h1> Câbles - RNT</h1> 
            </header>
            <?php if ($put_csv_screen) {?>
    <div id="alert_zone">
    <h2> Toutes les données existantes vont être supprimées ! </h2>
    <form enctype="multipart/form-data" action="./index.php" method="POST">
    Indiquer le fichier à uploader pour <strong>remplacer</strong> le contenu 
    du cahier de câbles (données associées exceptées) : 
    <br /><input type="hidden" name="command" value="put_csv_confirmed">
    <input type="file" name="src_csv" />
    <br/>
    <input type="submit" value="Uploader la liste au format CSV" />
    </form>
    </div>
    <p>
    <form action="./index.php" method="POST">
    <input type="submit" value="Annuler...">
    </form>
    </p>

<?php }
    elseif ($add_csv_screen) {?>
    <form enctype="multipart/form-data" action="./index.php" method="POST">
    Indiquer le fichier à uploader pour <strong>ajouter</strong> au contenu 
    du cahier de câbles (données associées exceptées) : 
    <br /><input type="hidden" name="command" value="add_csv_confirmed">
    <input type="file" name="src_csv" />
    <br/>
    <input type="submit" value="Uploader la liste au format CSV" />
    </form>
    <p>
    <form action="./index.php" method="POST">
    <input type="submit" value="Annuler...">
    </form>
    </p>
<?php }
    else { ?>
            <div id="query_zone">
                <?php include("include/query.php"); 
            echo "Nombre de câbles : " . $db->get_cable_total_number();
            ?>
            </div>  
            <div id="display_zone">
                <?php
                    if ($db->message != "") {
                        echo "<div id=\"message_zone\">" . $db->message .
                            "</div>\n";

                    }
                    if ($mode == "list") {
                        include("include/list.php");
                    }
    }
                ?>
            </div>
    </body>
</html>

<?php
   $db->close_db();
?>
