<?php


?>
<div id="mode_chooser"
<?php
if ($_SESSION["rw"]) {
    echo " class=\"rw_mode\"";
}
else {
    echo " class=\"ro_mode\"";
}
?>
>
<div class="top_button">
<?php
    if ($_SESSION["rw"]) {
        echo "mode : lecture-écriture";
    }
    else {
        echo "mode : lecture seule";
    }
?>
</div>
<div class="bottom_button">
<?php
    if (!$_SESSION["rw"]) { ?>
        <form action="./index.php" method="POST">
            <input type="hidden" name="command" value="rw">
            <input type="submit" value="activer l'écriture">
        </form>
    <?php }
    else {?>
        <form action="./index.php" method="POST">
            <input type="hidden" name="command" value="ro">
            <input type="submit" value="désactiver l'écriture">
        </form>

    <?php }
?>
</div>

<div class="filter_zone">
<?php
    # Préparation des champs si déjà remplis
    $filter_fields = Array("f_num_from", 
    "f_extremity", "f_cable_type", "f_order", "f_number"
                       );
    $filter = Array();
    foreach($filter_fields as $f) {
        if (isset($_POST[$f])) {
            $filter[$f] = $_POST[$f];
        }
        else 
            $filter[$f] = "";
    }
?>

<form action="./index.php" method="POST">
    <input type="hidden" name="command" value="filter">
    Numéro de câble à partir de :<br/>
    <input type="text" name="f_num_from" 
        value="<?php echo $filter["f_num_from"]?>"
        size="2"><br/>
    Localisation départ ou arrivée :<br/>
    <input type="text" name="f_extremity" 
        value="<?php echo $filter["f_extremity"]?>"
        size="10"><br/>
    Type :<br/>
    <input type="text" name="f_cable_type" 
        value="<?php echo $filter["f_cable_type"]?>"
        size="10"><br/>
    Classer par :
    <select name="f_order">
        <option value="cable_id"
        <?php if ($filter["f_order"] == "cable_id") echo "selected" ?>
        >Numéro</option>
        <option value="anti_cable_id"
        <?php if ($filter["f_order"] == "anti_cable_id") echo "selected" ?>
        >Numéro (inv)</option>
        <option value="last_modif"
        <?php if ($filter["f_order"] == "last_modif") echo "selected" ?>
        >Date de dernière modif</option>
        <option value="anti_last_modif"
        <?php if ($filter["f_order"] == "anti_last_modif") echo "selected" ?>
        >Date de dernière modif (inv)</option> 
    </select><br/>
    Afficher :
    <select name="f_number">
        <option value="10"
        <?php if ($filter["f_number"] == "10") echo "selected" ?>
        >10</option>
        <option value="50"
        <?php if ($filter["f_number"] == "50") echo "selected" ?>
        >50</option>
        <option value="100"
        <?php if ($filter["f_number"] == "100") echo "selected" ?>
        >100</option>
    </select><br/>
    <input type="submit" value="Rechercher">
</form>
</div>

</div>


<?php 
    if ($_SESSION["rw"]) {
?>
<form action="./index.php" method="POST">
Nouveau câble : <br>
<input type="text" name="new_num" size="3"> 
<input type="hidden" name="command" value="new_cable">
<input type="hidden" name="mode" value="rw">
<input type="submit" value="Envoi">
</form>

<br/>
<form action="./index.php" method="POST">
<input type="hidden" name="command" value="clone">
Cloner câble 
<input type="text" name="src_id" size="4" 
<?php if (isset($_POST["src_id"])) {
    echo "value=\"" . $_POST["src_id"] ." \"";
}
?>
>
<br/>
en temps que nouveau câble numéro : 
<input type="text" name="tgt_id" size="4">
<input type="submit" value="Cloner">
</form>
<?php
    }
?>
<p>
<form action="./index.php" method="POST">
<input type="hidden" name="command" value="get_csv">
<input type="submit" value="Télécharger la liste en CSV">
</form>
</p>
<?php
    if ($_SESSION['rw']) { 
?>
<p>
<form action="./index.php" method="POST">
Attention : la commande suivante
<strong>REMPLACE</strong> la base de données !
<input type="hidden" name="command" value="put_csv">
<input type="submit" value="Uploader la liste en CSV">
</form>
</p>
<p>
<form action="./index.php" method="POST">
Attention : la commande suivante
<strong>AJOUTE</strong> des entrées à la base de données 
<input type="hidden" name="command" value="add_csv">
<input type="submit" value="Ajouter des câbles en CSV">
</form>
</p>
<?php }
?>
