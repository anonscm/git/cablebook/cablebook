<?php
include_once('./config/settings.php');

class MyDB {
    private $db;
    public $message;
    public $default_query;

    function __construct() {
        $this->default_query = "SELECT * FROM " . CABLE_TABLE . " ORDER BY id";
        $this->default_query .= " LIMIT 10;"; 

        $this->CSV_FIELDS = Array("id", "usage",
            "type", "conn", "length", "zc", "delay",
            "end1", "end2", "comment", "lastmod", "author");
    }
        
    function connect_to($dbpath) {
        # Connection à la base de données / Initialisation de la chaine
        # d'affichage
        $this->db = new SQLite3($dbpath);
        $this->message = "";
    }

    function get_cable_total_number() {
        $str = "SELECT count(*)  FROM " . CABLE_TABLE .";";
        $res = $this->db->query($str);
        $num = $res->fetchArray();
        return $num[0];

    }

    function new_cable($new_num) {
        # Ajoute un nouveau câble à la base de données

        # Vérifie que $new_num est un nouveau numéro
        if ($new_num == "") {
            $this->message = "Erreur : entrez un nouveau numéro"; 
            return False;
        }

        $vt = gettype($new_num); 
        if ($vt =! "integer") {
            $this->message = "Erreur : " . $new_num . 
                " n'est pas un numéro valide"; 
            return False;
        }
        
        $str = "SELECT * FROM " . CABLE_TABLE . " WHERE id='$new_num'";
        $res = $this->db->query($str);
        if ($res->fetchArray()) {
            $this->message .= "Erreur : le câble " . $new_num .
                " existe déjà<br/>";
            return False;
        };

        # Tout est OK, on ajoute le câble
        $str = "INSERT INTO " . CABLE_TABLE ." VALUES (";
        $str .= $new_num; #id
        $str .= ", null"; # type
        $str .= ", null"; # conn
        $str .= ", null"; # length
        $str .= ", null"; # zc
        $str .= ", null"; # delay
        $str .= ", null"; # end1
        $str .= ", null"; # end2
        $str .= ", null"; # usage
        $str .= ", '" . $_SESSION["user_id"]. "'"; # author
        $str .= ", null"; # comments
        $str .= ", DateTime('now')"; # lastmod
        $str .= ');';
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:" . $this->db->lastErrorMsg();
            $this->message .= ", requête = " . $str;
        }

        # On règle la requête de manière à indiquer le nouveau câble en premier
        $_SESSION["query"] = "SELECT * FROM " . CABLE_TABLE . " WHERE id>=";
        $_SESSION["query"] .= "'" . $new_num . "' ORDER BY id LIMIT 10;"; 
        return True;

    }
 
    function clone_cable($post) {
        # Ajoute un nouveau câble à la base de données à partir des données
        # d'un câble existant


        $source = $this->get_cable($post["src_id"]);
        $this->new_cable($post["tgt_id"]);

        $fields = Array("type", "conn", "length", "zc", "delay", "end1", 
            "end2", "usage", "comment");


        $str = "UPDATE " . CABLE_TABLE . " SET ";
        foreach($fields as $f) {
            $str .= $f . "='" . $this->db->escapeString($source[$f]) . "',";
        }
        $str .= "lastmod=DateTime('now'),"; 
        $str .= "author='" . $_SESSION["user_id"] . "'";
        $str .= " WHERE id='". $post["tgt_id"] . "';";
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:" . $this->db->lastErrorMsg();
            $this->message .= "\n<br/>SQL query =" . $str;
            return False;
        }

        return True;

    }


    function delete_cable($cable_id) {
        # Effacement d'un câble, des fichiers attachés et du répertoire de 
        # données associées le cas échéant
        # Récupération de la liste des fichiers attachés
        $data = $this->get_data($cable_id);
        foreach($data as $d) {
            $this->delete_data($d["id"]);
        }
        # Effacement du répertoire (censément vide)
        $cabdir_name = sprintf("cable_%04d",$cable_id);
        $data_path = SITEROOT . "/" . DATASUBDIR . '/' . $cabdir_name;
        if (file_exists($data_path)) {
            rmdir($data_path);
        }
        # Effacement du câble dans la base de données
        $str = "DELETE FROM ". CABLE_TABLE . " WHERE id='" . $cable_id ."';";
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:"; 
            $this->message .= $this->db->lastErrorMsg();
            $this->message .= "\nSQL query =" . $str;
        }
        return true;
    }

    function get_list($query) {
        # Récupère l'ensemble des câbles présents et les renvoie comme
        # un array PHP
        $output = Array();
        $res = $this->db->query($query);
        if (!$res) {
            echo "Erreur SQL sur la requête :" . $query;
            echo ", requête par défaut";
            $res = $this->db->query($this->default_query);
        }
        while ($buf = $res->fetchArray(SQLITE3_ASSOC)){
            $output[] = $buf;
        }
        return $output;
    }

    function delete_data($data_id) {
        # Effacement d'un fichier attaché et de l'entrée DB correspondante
        # Récupération de l'URL pour reconstituer le chemin d'accès
        $str = "SELECT url FROM ". DATA_TABLE . " WHERE";
        $str .= " id='" . $data_id ."';";
        $res = $this->db->query($str);
        $d = $res->fetchArray(SQLITE3_ASSOC);

        # Effacement de l'entrée en base de données
        $str = "DELETE FROM " . DATA_TABLE . " WHERE";
        $str .= " id='" . $data_id ."';";
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:"; 
            $this->message .= $this->db->lastErrorMsg();
            $this->message .= "\nSQL query =" . $str;
        }
        # Effacement du fichier correspondant
        unlink(SITEROOT . str_replace("./","/",$d["url"])); 

        return True;
    }

    function build_from_file($post, $files, $add_flag) {
        # Reconstruit la base de données "cables" à partir d'un fichier CSV 
        # fourni

        $fields = $this->CSV_FIELDS;
        $input_cables = Array();

        if (isset($files["src_csv"]["tmp_name"])) {
            if ($files["src_csv"]["tmp_name"] != "") {
                # Scan the uploaded CSV file
                if (($handle = fopen($files["src_csv"]["tmp_name"], "r")) !== FALSE) {
                    while (($data = fgetcsv($handle,0, ";")) !== FALSE) {
                        # Elimination ligne commentaires
                        if ($data[0] == "id") {
                            continue;
                        }
                        # Stockage en mémoire, dans le bon ordre
                        $buf = Array();
                        $nfields = count($fields);
                        $ndata = count($data);
                        for($i=0;$i<$nfields;$i+=1) {
                            # On met la bonne valeur si elle existe, 'null' sinon 
                            if ($i<$ndata) {
                                $buf[$fields[$i]] = $this->db->escapeString($data[$i]);
                            }
                            else {
                                $buf[$fields[$i]] = "null";
                            }
                        }
                        $input_cables[] = $buf;
                    }
                }
                fclose($handle);
                if (!$add_flag) {
                    # On a tout lu sans détecter de problème, donc 
                    # maintenant on peut effacer les anciens câbles
                    $str_delete_all = "DELETE FROM " . CABLE_TABLE . ";";
                    $this->db->exec($str_delete_all);
                }
                # Remplissage de la base de données à partir du contenu de $input_cables
                foreach ($input_cables as $cable_params) {
                    $this->put_cable($cable_params);
                }
            }
        }
    }

    function maj($post, $files) {
        # Mise à jour des champs ci dessous
        $field_list = Array("type", "conn", "length", "zc", "delay", "end1", 
            "end2", "usage", "comment");

        $str = "UPDATE " . CABLE_TABLE . " SET ";
        foreach ($field_list as $field) {
            $buf = $this->db->escapeString($post["cable_" . $field]);
            $str .= $field . "='" . $buf . "',";
        }
        # On met aussi à jour le timestamp et l'identifiant de celui
        # qui fait la modif
        $str .= "lastmod=DateTime('now'),"; 
        $str .= "author='" . $_SESSION["user_id"] . "'";
        $str .= " WHERE id='" . $post["cable_id"] . "';";
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:" . $this->db->lastErrorMsg();
            $this->message .= "\nSQL query =" . $str;
        }
        # Cas particulier : ajout d'un fichier attaché
        if (isset($files["to_be_uploaded"]["tmp_name"])) {
            if ($files["to_be_uploaded"]["tmp_name"] != "") {
                $this->add_attached_file($post, $files);
            }
        }
        # Mise à jour des informations des fichiers attachés
        foreach($post as $key => $value) {
            # On scanne les POST pour trouver ceux se rattachant aux données
            if(strpos($key, "data") !== false) {
                $str = "";
                if(strpos($key, "type") !== false) {
		    $myid = explode("_",$key); 
                    $str = "UPDATE ". DATA_TABLE . " SET ";
                    $str .= "type='" . $value . "'"; 
                    $str .= " WHERE id='" . $myid[1] . "'";
                }
                if(strpos($key, "comment") !== false) {
		    $myid = explode("_",$key);
                    $str = "UPDATE ". DATA_TABLE . " SET ";
                    $str .= "comment='" . $value . "'"; 
                    $str .= " WHERE id='" . $myid[1] . "'";
                }
                if ($str != "") {
                    if (!$this->db->exec($str)) {
                        $this->message = "Erreur sql:"; 
                        $this->message .= $this->db->lastErrorMsg();
                        $this->message .= "\nSQL query =" . $str;
                    }
                } 
            }
        }
        # Effacement des fichiers attachés marqués
        foreach($post as $key => $value) {
            if(strpos($key, "data") !== false) {
                if(strpos($key, "delete") !== false) {
                    # data_id récupéré entre "_"
		    $myid = explode("_",$key);
                    $this->delete_data($myid[1]);
                }
            }
        }


        return True;
        

    }

    function get_data($cable_id) {
        # Récupération de toutes les données associées à un câble
        $query = "SELECT * FROM " . DATA_TABLE . " WHERE cable_id='" .
            $cable_id . "';";
        $output = Array();
        $res = $this->db->query($query);
        while ($buf = $res->fetchArray(SQLITE3_ASSOC)){
            $output[] = $buf;
        }
        return $output;
    }


    function get_cable($cable_id) {
        # Récupération des champs d'un cable en particulier
        
        # Vérifie que le cable existe
        $vt = gettype($cable_id); 
        if ($vt =! "integer") {
            $this->message = "Erreur : " . $cable_id . 
                " n'est pas un numéro valide"; 
            return False;
        }
        
        $str = "SELECT * FROM " . CABLE_TABLE . " WHERE id='$cable_id'";
        $res = $this->db->query($str);
        $output = $res->fetchArray(SQLITE3_ASSOC);
        if ($output) {
            return $output;
        }
        else {
            $this->message = "Erreur : le câble n'existe pas";
            return False;
        };

    }

    function put_cable($cable_params) { 
        # Création du nouveau câble
        $this->new_cable($cable_params["id"]);

        # Mise à jour des valeurs
        $str = "UPDATE " . CABLE_TABLE . " SET ";
        foreach ($this->CSV_FIELDS as $field) {
            if ($field == "id") {
                continue;
            }
            $buf = $this->db->escapeString($cable_params[$field]);
            $str .= $field . "='" . $buf . "',";
        }
        # On met aussi à jour le timestamp et l'identifiant de celui
        # qui fait la modif
        $str .= "lastmod=DateTime('now'),"; 
        $str .= "author='" . $_SESSION["user_id"] . "'";
        $str .= " WHERE id='" . $cable_params["id"] . "';";
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:" . $this->db->lastErrorMsg();
            $this->message .= "\nSQL query =" . $str;
        }
    }

    function add_attached_file($post, $files) {
        # Ajout d'un fichier attaché
        #var_dump($files);

        # On vérifie si le sous répertoire du câble existe, sinon on le crée.
        $cabdir_name = sprintf("cable_%04d",$post["cable_id"]);
        $data_path = SITEROOT . "/" . DATASUBDIR . '/' . $cabdir_name;
        if (!file_exists($data_path)) {
            mkdir($data_path);
        }

        # Fabrication du nom du fichier destination
        #print($files["to_be_uploaded"]["name"]);
        $path_parts = pathinfo($files["to_be_uploaded"]["name"]);
        $dest_name_base = $path_parts["filename"];
        if (isset($path_parts["extension"])) {
            $dest_name_ext = $path_parts["extension"];
        }
        else {
            $dest_name_ext = "";
        }
        # Si le fichier destination existe, on ajoute un index.
        $target = $data_path . "/" . $dest_name_base; 
        if ($dest_name_ext != "") {
            $target .= "." . $dest_name_ext;
        }
        $i = 0;
        while (file_exists($target)) {
                $i += 1;
                $target = $data_path . "/" . $dest_name_base . "_$i";
            if ($dest_name_ext != "") {
                $target .= "." . $dest_name_ext;
            }
        }

        
        # Upload
        if (!move_uploaded_file($files["to_be_uploaded"]["tmp_name"], 
                $target)) {
            $this->message = "Echec d'upload du fichier";
            return False;
        }

        # Ajout du lien dans la base de données "data"
        $str = "INSERT INTO " . DATA_TABLE ." VALUES (";
        $str .= "null";
        $str .= ", " . $post["cable_id"]; # cable_id
        $str .= ", null"; # type
        $str .= ", '." . str_replace(SITEROOT,
                                           "",
                                           $target) . "'"; # url
        $str .= ", DateTime('now')"; # date
        $str .= ", '" . $_SESSION["user_id"]. "'"; # author
        $str .= ", null"; # comments
        $str .= ", DateTime('now')"; # lastmod
        $str .= ');';
        if (!$this->db->exec($str)) {
            $this->message = "Erreur sql:" . $this->db->lastErrorMsg();
        }
    }

    function filter_query($post) {
        # Construction de la requête d'après les éléments du formulaire 
        # query.php, transmis par méthode POST
        $query = "SELECT * FROM " . CABLE_TABLE . " WHERE ( ";
        # Numéro de départ
        if ($post["f_num_from"] != "") {
            $num_from = (int) $post["f_num_from"];
            if (is_int($num_from)
                    and ($num_from < 9999)) {
                $query .= "(id >= '" . $num_from ."')";
            }
            else {
                $this->message = "Erreur : num_from = " . $post["f_num_from"]; 
                $this->message .= "> 9999 or not int";
                $query .= "(id >= '0')";
            }
        }
        else {
            $query .= "(id >= '0')";
        }
        # Extrémité
        $extremity = $this->db->escapeString(trim($post["f_extremity"]));
        $post["f_extremity"] =  $extremity;
        if ($extremity != "") {
            $query .= " AND ( (end1 LIKE '%" . $extremity . "%')"; 
            $query .= " OR (end2 LIKE '%" . $extremity . "%') ) ";
        }
        # Type
        $cable_type = $this->db->escapeString(trim($post["f_cable_type"]));
        $post["f_cable_type"] =  $cable_type;
        if ($cable_type != "") {
            $query .= " AND (type LIKE '%" . $cable_type . "%') "; 
        }
        # Ordre
        $query .= ") ";
        switch ($post["f_order"]) {
            case "cable_id": 
                $query .= "ORDER BY id ";
                break;
            case "anti_cable_id": 
                $query .= "ORDER BY id DESC ";
                break;
            case "last_modif": 
                $query .= "ORDER BY lastmod ";
                break;
            case "anti_last_modif": 
                $query .= "ORDER BY lastmod DESC ";
                break;
        } 
        # Nombre de câbles affichés
        $query .= "LIMIT " . $post["f_number"] . ";";
        return $query;        
    }

    function dump_csv() {
        # Récupère l'ensemble des câbles présents et les renvoie comme
        # un fichier csv
        $query = "SELECT * FROM " . CABLE_TABLE . " ORDER BY id;";
        $res = $this->db->query($query);
        if (!$res) {
            echo "Erreur SQL sur la requête :" . $query;
            echo ", requête par défaut";
            $res = $this->db->query($this->default_query);
        }
        $csv_sep = ";";
	date_default_timezone_set('Europe/Paris');
        $filename = "cahier_cables_" . strftime("%G-%m-%dT%H.%M.%S.csv");
        # Header pour fichier CSV servi par apache
        header("Content-type: text/csv");  
        header("Cache-Control: no-store, no-cache");  
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        $fields = $this->CSV_FIELDS; # Defined in settings.php
        foreach ($fields as $f) {
            echo $f . $csv_sep;
        }
        echo "\n";
        while ($buf = $res->fetchArray(SQLITE3_ASSOC)){
            foreach ($fields as $f) {
                if (is_numeric($buf[$f])) {
                    echo $buf[$f] . $csv_sep;
                }
                else {
                    echo "\"" . str_replace("\"","\"\"",$buf[$f]) . "\"" .
                        $csv_sep;
                }
            }
         echo "\n";   
        }
        return Null;
    }

    function close_db() {
        $this->db->close();
    }
}

?>
