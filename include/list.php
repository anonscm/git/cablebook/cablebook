<?php


# Boucle sur le tableau de câbles récupéré dans index.php
# Chaque élément est un tableau associatif (noms des champs = noms de la 
# colonne dans la BD

if (count($cable_list) == 0) {
    echo "Aucun câble ne correspond aux critères";
}

foreach ($cable_list as $cable) {?>
<div class="cable">
    <?php
    # Si on est en mode de modification, on regarde si ce cable est 
    # concerné et on grey-out si non.
    $greyout = "";
    $edit = False;
    if ($modify_cable_mode) {
        if ($cable["id"] == $modify_cable_id) {
            $edit = True;?>        
    <form action="./index.php" enctype="multipart/form-data" method="POST">
        <?php
        }
        else {
            $greyout = "disabled=\"disabled\"";
        }
    }


?>

    <a name="anchor_<?php echo $cable["id"]?>"/>
    <div class="top_cable">
        <div class="cid"> <?php 
            # Le numéro du cable - non éditable
            echo $cable["id"] ?></div>
        <div class="usage"><?php affiche_usage($cable, $edit)?></div>
    </div>
    
    <div class="middle_cable">
        <div class="cend"><?php 
        affiche_depart_arrive($cable, $edit)?></div>
        <div class="carac"><?php 
        affiche_caracteristiques($cable, $edit)?></div>
        <div class="comment"><?php affiche_comment($cable, $edit) ?> </div>
    </div>
    <div class="data"> <?php 
        # Récupération des fichiers attachés
        $data = $db->get_data($cable["id"]); 
        affiche_data($cable,$data,$edit) ?>
    </div>

<?php if ( ($_SESSION["rw"]) and (!$edit)) {
    # A n'afficher qu'en mode rw
    # Grisé si en mode édition d'un autre cable
        ?>
        <div class="modif">
            <form action="./index.php#anchor_<?php 
                echo $cable["id"];
            ?>" method="POST">
            <input type="hidden" name="id" value="<?php echo $cable["id"] ?>">
            <input type="hidden" name="command" value="modify">
            <input type="submit" value="Modifier" <?php echo $greyout;?>>
            </form>
        </div>
    <?php } 
    if ($edit) {
    ?>
    <input type="hidden" name="cable_id" value="<?php echo $cable["id"]?>">
    <input type="hidden" name="command" value="maj">
    <input type="submit" value="Enregistrer les modifications">
    </form> <?php
    } ?>
    <div class="edit_info footer"> 
    Dernière modif : <?php echo $cable["lastmod"] ?>
        par : <?php echo $cable["author"]?>
    <?php if ($edit){ ?>
        <br/>
        <form action="./index.php" method="POST">
        <input type="hidden" name="cable_id" value="<?php echo $cable["id"] ?>">
        <input type="hidden" name="command" value="delete">
        Supprimer ce câble et tous ses fichiers attachés : 
        <input type="checkbox" name="delete">
        <input type="submit" value="Supprimer">
        </form>


    <?php } ?>
    </div>
    
</div>
<?php
}

?>
