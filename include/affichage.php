<?php
function safe_disp($str) {
    if ($str == "") {
        $str = "--";
    }
    return htmlspecialchars($str);
}
function affiche_comment($cable, $edit) {
    if ($edit) {?>
        Remarques : <br>
        <textarea name="cable_comment" cols="50" rows="3" ><?php 
        echo $cable["comment"]; ?></textarea>
    
    <?php
    }
    else {
        if ($cable["comment"] != "") {
            echo "<p>Remarques : " ;
            echo safe_disp($cable["comment"]) ."</p>"; 
        }

    }
}

function affiche_usage($cable, $edit) {
    if ($edit) {?>
        <textarea name="cable_usage" cols="60" rows="2" ><?php 
        echo $cable["usage"]; ?></textarea>
    <?php
    }
    else {
        echo safe_disp($cable["usage"]); 

    }
}
function affiche_caracteristiques($cable, $edit) {
    echo "<div class=\"carac\">"; 
    echo "<table><tr>";
    echo "<th>Type</th><th>Conn.</th><th>Zc (Ω)</th>";
    echo "<th>Retard (ns)</th><th>Long. (m)</th>";
    echo "</tr>\n<tr><td>";
    if ($edit) {?>
<input type="text" name="cable_type" value="<?php echo $cable["type"];?>">
    <?php }
    else {
        echo safe_disp($cable["type"]); 
    }
    
    echo "</td><td>"; 
    if ($edit) {?>
<input type="text" name="cable_conn" value="<?php echo $cable["conn"];?>">
    <?php }
    else {
        echo safe_disp($cable["conn"]); 
    }
    
    echo "</td><td>"; 
    if ($edit) {?>
<input type="text" name="cable_zc" value="<?php echo $cable["zc"];?>">
    <?php }
    else {
        echo safe_disp($cable["zc"]); 
    }
    
    echo "</td><td>"; 
    if ($edit) {?>
<input type="text" name="cable_delay" value="<?php echo $cable["delay"];?>">
    <?php }
    else {
        echo safe_disp($cable["delay"]); 
    }
    
    echo "</td><td>"; 
    if ($edit) {?>
<input type="text" name="cable_length" value="<?php echo $cable["length"];?>">
    <?php }
    else {
        echo safe_disp($cable["length"]); 
    }
    echo "</td></tr></table></div>\n";
}

function affiche_depart_arrive($cable, $edit) {
    echo "<table>";
    echo "<tr><th>Départ</th><th>Arrivée</th></tr>\n";
    echo "<tr><td>";
    if ($edit) {?>
        <input type="text" name="cable_end1" value="<?php 
        echo $cable["end1"]
        ?>">
    <?php
    }
    else {
        echo safe_disp($cable["end1"]); 
    }
    echo "</td><td>";
    if ($edit) {?>
        <input type="text" name="cable_end2" value="<?php 
        echo $cable["end2"]
        ?>">
    <?php
    }
    else {
        echo safe_disp($cable["end2"]); 
    }
    echo "</td></tr></table>\n";
}

function affiche_data($cable, $data, $edit) {
    if (!$data) {
        #echo "Pas de données associées";
    }
    else {
        foreach($data as $d) {
            echo "<div class=\"data_zone\">\n";
            echo "<a target=\"_new\" href=\"" . $d["url"] . "\">" . 
                basename($d["url"]) ."</a>";
            if ($edit) {?>
                Effacer ce fichier : <input type="checkbox" 
name="data_<?php echo $d["id"] ?>_delete"/>
            <?php }
           
           echo "<br/>" . 
                $d["date"] . " / " . $d["author"] .  "";
            echo "<p>Type :";
            if ($edit) {?>
                <input type="text" name="data_<?php echo $d["id"] ?>_type"
                value="<?php  echo safe_disp($d["type"]) ?>" />
                
            <?php }
            else {
                echo safe_disp($d["type"]);
            }

            echo "<br/>Remarques :";
            if ($edit) {?>
                <input type="text" name="data_<?php echo $d["id"] ?>_comment"                value="<?php echo safe_disp($d["comment"])?>"/>
                
            <?php }
            else {
                echo safe_disp($d["comment"]);
            }

            echo "</p>";
            echo "</div>\n";
        }
    }
    if ($edit) {?>
        <p>
        Ajouter un fichier attaché : 
        <input type="file" name="to_be_uploaded" />
        </p>
        
<?php
    }
}

?>
