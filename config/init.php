<?php
include('./settings.php');

$db = new SQLite3(DBPATH);

$str = 'CREATE TABLE ' . CABLE_TABLE;
$str .= ' (id int';
$str .= ', type text';
$str .= ', conn text';
$str .= ', length text';
$str .= ', zc real';
$str .= ', delay real';
$str .= ', end1 text';
$str .= ', end2 text';
$str .= ', usage text';
$str .= ', author text';
$str .= ', comment text';
$str .= ', lastmod DATETIME DEFAULT CURRENT_TIMESTAMP';
$str .= ')';
$db->exec($str);

$str = 'CREATE TABLE ' . DATA_TABLE;
$str .= ' (id INTEGER PRIMARY KEY';
$str .= ', cable_id int';
$str .= ', type text';
$str .= ', url text';
$str .= ', date int';
$str .= ', author text';
$str .= ', comment text';
$str .= ', lastmod DATETIME DEFAULT CURRENT_TIMESTAMP';
$str .= ')';
$db->exec($str);


$str = 'CREATE TABLE ' . SETTINGS_TABLE;
$str .= ' (id int';
$str .= ', varname text';
$str .= ', value text';
$str .= ')';
$db->exec($str);


?>
